package com.tobias.bankservice.exception;

import com.tobias.bankservice.dto.ResponseErrorDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { BankException.class })
    protected ResponseEntity<ResponseErrorDto> handleDemoAppException(BankException exception, WebRequest request) {
        log.error("Exception '{}' occurred. Message: {}", exception.getStatus(), exception.getErrorMessage());

        ResponseErrorDto responseErrorDto = new ResponseErrorDto();
        responseErrorDto.setMessage(exception.getErrorMessage());

        return ResponseEntity.status(exception.getStatus()).body(responseErrorDto);
    }
}
