package com.tobias.bankservice.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BankException extends Exception {

    private final HttpStatus status;
    private final String errorMessage;

    public BankException(HttpStatus status, String message) {
        this.status = status;
        this.errorMessage = message;
    }
}
