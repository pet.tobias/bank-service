package com.tobias.bankservice.repository;

import com.tobias.bankservice.entity.CardHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CardHolderRepository extends JpaRepository<CardHolder, Long> {

    Optional<CardHolder> findByCardNumber(String cardNumber);
}
