package com.tobias.bankservice.dto;

import com.tobias.bankservice.entity.TransactionType;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptDTO {

    private TransactionType transactionType;
    private BigDecimal amount;
    private BigDecimal newBalance;
}
