package com.tobias.bankservice.dto;

import com.tobias.bankservice.entity.AuthMethod;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CardHolderAuthMethodDTO {

    private String cardNumber;
    private AuthMethod authMethod;
}
