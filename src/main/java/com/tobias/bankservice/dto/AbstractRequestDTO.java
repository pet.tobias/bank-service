package com.tobias.bankservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AbstractRequestDTO {

    @Pattern(regexp = "[0-9]+")
    private String cardNumber;
}
