package com.tobias.bankservice.entity;

public enum TransactionType {
    WITHDRAWAL,
    DEPOSIT
}
