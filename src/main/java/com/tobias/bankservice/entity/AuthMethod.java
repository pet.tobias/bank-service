package com.tobias.bankservice.entity;

public enum AuthMethod {
    FINGER_PRINT,
    PIN
}
