package com.tobias.bankservice.service;

import com.tobias.bankservice.dto.AccountBalanceDTO;
import com.tobias.bankservice.dto.ReceiptDTO;
import com.tobias.bankservice.dto.TransactionRequestDTO;
import com.tobias.bankservice.entity.Account;
import com.tobias.bankservice.entity.Transaction;
import com.tobias.bankservice.entity.TransactionType;
import com.tobias.bankservice.exception.BankException;
import com.tobias.bankservice.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor
public class AccountService {

    private final CardHolderService cardHolderService;
    private final AccountRepository accountRepository;

    public AccountBalanceDTO withdrawMoney(TransactionRequestDTO transactionDTO) throws BankException {
        Account account = cardHolderService.findByCardNumber(transactionDTO.getCardNumber()).getAccount();
        BigDecimal newBalance = account.getBalance().subtract(transactionDTO.getAmount());
        if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new BankException(HttpStatus.BAD_REQUEST, "Insufficient funds");
        }
        updateAccountAfterTransaction(TransactionType.WITHDRAWAL, account, transactionDTO.getAmount(), newBalance);
        return getBalance(account);
    }

    public AccountBalanceDTO depositMoney(TransactionRequestDTO transactionDTO) throws BankException {
        Account account = cardHolderService.findByCardNumber(transactionDTO.getCardNumber()).getAccount();
        BigDecimal newBalance = account.getBalance().add(transactionDTO.getAmount());
        updateAccountAfterTransaction(TransactionType.DEPOSIT, account, transactionDTO.getAmount(), newBalance);
        return getBalance(account);
    }

    public AccountBalanceDTO getBalance(Account account) {
        return new AccountBalanceDTO(account.getBalance());
    }

    public ReceiptDTO printReceipt(String cardNumber) throws BankException {
        Account account = cardHolderService.findByCardNumber(cardNumber).getAccount();
        if (account.getLastTransaction() == null) {
            throw new BankException(HttpStatus.NOT_FOUND, "No transactions found");
        }
        return ReceiptDTO.builder()
                .transactionType(account.getLastTransaction().getTransactionType())
                .newBalance(account.getLastTransaction().getNewBalance())
                .amount(account.getLastTransaction().getAmount())
                .build();
    }

    private void updateAccountAfterTransaction(TransactionType type, Account account, BigDecimal amount, BigDecimal newBalance) {
        if (account.getLastTransaction() == null) {
            account.setLastTransaction(Transaction.builder()
                    .transactionType(type)
                    .amount(amount)
                    .newBalance(newBalance)
                    .build());
        } else {
            account.getLastTransaction().setTransactionType(type);
            account.getLastTransaction().setAmount(amount);
            account.getLastTransaction().setNewBalance(newBalance);
        }
        account.setBalance(newBalance);
        accountRepository.save(account);
    }
}
