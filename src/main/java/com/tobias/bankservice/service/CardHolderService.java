package com.tobias.bankservice.service;

import com.tobias.bankservice.dto.CardHolderAuthMethodDTO;
import com.tobias.bankservice.dto.CardHolderAuthMethodRequestDTO;
import com.tobias.bankservice.dto.CardHolderLoginDTO;
import com.tobias.bankservice.entity.AuthMethod;
import com.tobias.bankservice.entity.CardHolder;
import com.tobias.bankservice.exception.BankException;
import com.tobias.bankservice.repository.CardHolderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor
public class CardHolderService {

    private final CardHolderRepository cardHolderRepository;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public CardHolderAuthMethodDTO verifyCardNumber(String cardNumber) throws BankException {
        CardHolder cardHolder = findByCardNumber(cardNumber);
        return new CardHolderAuthMethodDTO(cardNumber, cardHolder.getAuthMethod());
    }

    public void attemptLogin(CardHolderLoginDTO loginDTO) throws BankException {
        CardHolder cardHolder = findByCardNumber(loginDTO.getCardNumber());
        if (validatePassword(loginDTO.getPassword(), cardHolder)) {
            cardHolder.setFailedAttempts(0);
            cardHolderRepository.save(cardHolder);
        } else {
            handleLoginFailure(cardHolder);
        }
    }

    public CardHolderAuthMethodDTO updateAuthMethod(CardHolderAuthMethodRequestDTO authMethodDTO) throws BankException {
        CardHolder cardHolder = findByCardNumber(authMethodDTO.getCardNumber());
        cardHolder.setAuthMethod(authMethodDTO.getAuthMethod());
        cardHolderRepository.save(cardHolder);
        return new CardHolderAuthMethodDTO(cardHolder.getCardNumber(), cardHolder.getAuthMethod());
    }

    public CardHolder findByCardNumber(String cardNumber) throws BankException {
        Optional<CardHolder> cardHolder = cardHolderRepository.findByCardNumber(cardNumber);
        if (cardHolder.isEmpty()) {
            log.error("card number: {} not found", cardNumber);
            throw new BankException(HttpStatus.NOT_FOUND, "card not found");
        }
        return cardHolder.get();
    }

    private void handleLoginFailure(CardHolder cardHolder) throws BankException {
        String message;
        if (cardHolder.getFailedAttempts() > 2) {
            cardHolder.setBlocked(true);
            log.error("card number: {} blocked after too many failed attempts", cardHolder.getCardNumber());
            message = "More than 3 failed attempts: Card is blocked";
        } else {
            cardHolder.setFailedAttempts(cardHolder.getFailedAttempts() + 1);
            log.error("card number: {} failed login", cardHolder.getCardNumber());
            message = "Invalid credentials";
        }
        cardHolderRepository.save(cardHolder);
        throw new BankException(HttpStatus.UNAUTHORIZED, message);
    }

    private boolean validatePassword(String password, CardHolder cardHolder) {
        String encodedPassword = cardHolder.getAuthMethod().equals(AuthMethod.PIN)
                ? cardHolder.getPin()
                : cardHolder.getFingerPrint();
        return passwordEncoder.matches(password, encodedPassword);
    }
}
