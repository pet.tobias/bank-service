package com.tobias.bankservice.controller;

import com.tobias.bankservice.dto.CardHolderAuthMethodDTO;
import com.tobias.bankservice.dto.CardHolderAuthMethodRequestDTO;
import com.tobias.bankservice.dto.CardHolderLoginDTO;
import com.tobias.bankservice.exception.BankException;
import com.tobias.bankservice.service.CardHolderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@Slf4j
@RequiredArgsConstructor
@RestController
@Validated
@RequestMapping(value = "api/card-holder")
public class CardHolderController {

    private final CardHolderService cardHolderService;

    @GetMapping("/{cardNumber}")
    public CardHolderAuthMethodDTO validateCardNumber(
            @PathVariable
            @Pattern(regexp = "[0-9]+")
            String cardNumber) throws BankException {
        log.info("card number: {} requested validation", cardNumber);
        CardHolderAuthMethodDTO response = cardHolderService.verifyCardNumber(cardNumber);
        log.info("card number: {} successfully validated", cardNumber);
        return response;
    }

    @PostMapping("/login")
    public void attemptLogin(@RequestBody CardHolderLoginDTO loginDTO) throws BankException {
        log.info("card number: {} attempted login", loginDTO.getCardNumber());
        cardHolderService.attemptLogin(loginDTO);
        log.info("card number: {} successfully logged in", loginDTO.getCardNumber());
    }

    @PutMapping("/auth-method")
    public CardHolderAuthMethodDTO updateAuthMethod(@Valid @RequestBody CardHolderAuthMethodRequestDTO authMethodDTO)
            throws BankException {
        log.info("card number: {} updating auth method", authMethodDTO.getCardNumber());
        CardHolderAuthMethodDTO response = cardHolderService.updateAuthMethod(authMethodDTO);
        log.info("card number: {} updated auth method to {}", authMethodDTO.getCardNumber(),
                authMethodDTO.getAuthMethod());
        return response;
    }
}
