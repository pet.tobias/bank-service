package com.tobias.bankservice.controller;

import com.tobias.bankservice.dto.AccountBalanceDTO;
import com.tobias.bankservice.dto.ReceiptDTO;
import com.tobias.bankservice.dto.TransactionRequestDTO;
import com.tobias.bankservice.exception.BankException;
import com.tobias.bankservice.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@Slf4j
@RequiredArgsConstructor
@RestController
@Validated
@RequestMapping(value = "api/account")
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/receipt/{cardNumber}")
    public ReceiptDTO getReceipt(
            @PathVariable
            @Pattern(regexp = "[0-9]+")
                    String cardNumber) throws BankException {
        log.info("card number: {} requested receipt", cardNumber);
        return accountService.printReceipt(cardNumber);
    }

    @PostMapping("/withdraw")
    public AccountBalanceDTO withdraw(@RequestBody @Valid TransactionRequestDTO transactionRequestDTO) throws BankException {
        log.info("card number: {} withdrawing money", transactionRequestDTO.getCardNumber());
        return accountService.withdrawMoney(transactionRequestDTO);
    }

    @PostMapping("/deposit")
    public AccountBalanceDTO deposit(@RequestBody @Valid TransactionRequestDTO transactionRequestDTO) throws BankException {
        log.info("card number: {} depositing money", transactionRequestDTO.getCardNumber());
        return accountService.depositMoney(transactionRequestDTO);
    }
}



