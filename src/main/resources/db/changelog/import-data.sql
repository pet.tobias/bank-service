INSERT INTO account (balance) VALUES
(0),
(0),
(0);

INSERT INTO card_holder (card_number, auth_method, failed_attempts, is_blocked, account_id, finger_print, pin) VALUES
('0000000000000000', 0, 0, false, 1, '$2a$10$jv7HCQ12UI.X1vwYCEgQMunEbHUGorTPWWOB2g2yqjjYOXswLim9G', '$2a$10$3wTx1YKNJbLsFgcWq9rWweVqXpe0eP3v7SzSD2Ajl8P5Ve1/pHljO'),
('1111111111111111', 1, 0, false, 2, '$2a$10$ckxvOKzoqD.gBgY5gBJMe.bD2PsK.MEpxJ23B5YtzYo/6nFzXaSdK', '$2a$10$6SE/RPR/7bLd3RxUdlpc8.AfsIExRNRI2sE8O0JvFllQe0NNynYWW'),
('2222222222222222', 0, 0, false, 3, '$2a$10$FrGtfjZFZfQ0m1Ur9C9Xee3o6SLNr15EgMwj8TCIGrfjZcym2z7Du', '$2a$10$G8J5lnNLXSlemla1isP2B.dyC81N7zhv95n98ByJek/5u0PC4vMba');
