package com.tobias.bankservice.service;

import com.tobias.bankservice.dto.CardHolderAuthMethodDTO;
import com.tobias.bankservice.dto.CardHolderAuthMethodRequestDTO;
import com.tobias.bankservice.dto.CardHolderLoginDTO;
import com.tobias.bankservice.entity.AuthMethod;
import com.tobias.bankservice.entity.CardHolder;
import com.tobias.bankservice.exception.BankException;
import com.tobias.bankservice.repository.CardHolderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class CardHolderServiceTest {

    @InjectMocks
    private CardHolderService cardHolderService;

    @Mock
    CardHolderRepository cardHolderRepository;

    private final String CARD_NUMBER = "123";

    @Test
    public void testVerifyCardNumber() throws BankException {

        CardHolder cardHolder = CardHolder.builder().authMethod(AuthMethod.PIN).build();

        when(cardHolderRepository.findByCardNumber(CARD_NUMBER)).thenReturn(Optional.ofNullable(cardHolder));

        CardHolderAuthMethodDTO authMethodDTO = cardHolderService.verifyCardNumber(CARD_NUMBER);

        assertEquals(CARD_NUMBER, authMethodDTO.getCardNumber());
        assertEquals(AuthMethod.PIN, authMethodDTO.getAuthMethod());
    }

    @Test
    public void testAttemptLoginValid() throws BankException {
        CardHolderLoginDTO loginDTO = new CardHolderLoginDTO("0000");
        loginDTO.setCardNumber(CARD_NUMBER);
        CardHolder cardHolder = CardHolder.builder()
                .authMethod(AuthMethod.PIN)
                .failedAttempts(1)
                .pin("$2a$10$3wTx1YKNJbLsFgcWq9rWweVqXpe0eP3v7SzSD2Ajl8P5Ve1/pHljO")
                .build();

        when(cardHolderRepository.findByCardNumber(CARD_NUMBER)).thenReturn(Optional.ofNullable(cardHolder));

        cardHolderService.attemptLogin(loginDTO);

        assertNotNull(cardHolder);
        assertEquals(0, cardHolder.getFailedAttempts());
    }

    @Test
    public void testAttemptLoginInvalid() throws BankException {
        CardHolderLoginDTO loginDTO = new CardHolderLoginDTO("0000");
        loginDTO.setCardNumber(CARD_NUMBER);
        CardHolder cardHolder = CardHolder.builder()
                .authMethod(AuthMethod.FINGER_PRINT)
                .failedAttempts(1)
                .pin("0000")
                .build();

        when(cardHolderRepository.findByCardNumber(CARD_NUMBER)).thenReturn(Optional.ofNullable(cardHolder));

        try {
            cardHolderService.attemptLogin(loginDTO);
        } catch (BankException e) {
            assertNotNull(cardHolder);
            assertEquals(2, cardHolder.getFailedAttempts());
        }
    }

    @Test
    public void testAttemptLoginInvalidMaxAttempts() throws BankException {
        CardHolderLoginDTO loginDTO = new CardHolderLoginDTO("0000");
        loginDTO.setCardNumber(CARD_NUMBER);
        CardHolder cardHolder = CardHolder.builder()
                .authMethod(AuthMethod.PIN)
                .failedAttempts(3)
                .pin("0000")
                .build();

        when(cardHolderRepository.findByCardNumber(CARD_NUMBER)).thenReturn(Optional.ofNullable(cardHolder));

        try {
            cardHolderService.attemptLogin(loginDTO);
        } catch (BankException e) {
            assertNotNull(cardHolder);
            assertTrue(cardHolder.isBlocked());
        }
    }

    @Test
    public void testUpdateAuthMethod() throws BankException {
        CardHolderAuthMethodRequestDTO requestDTO = new CardHolderAuthMethodRequestDTO(AuthMethod.FINGER_PRINT);
        requestDTO.setCardNumber(CARD_NUMBER);
        CardHolder cardHolder = CardHolder.builder()
                .authMethod(AuthMethod.PIN)
                .cardNumber(CARD_NUMBER)
                .build();

        when(cardHolderRepository.findByCardNumber(CARD_NUMBER)).thenReturn(Optional.ofNullable(cardHolder));

        CardHolderAuthMethodDTO authMethodDTO = cardHolderService.updateAuthMethod(requestDTO);

        assertNotNull(cardHolder);
        verify(cardHolderRepository).save(cardHolder);
        assertEquals(AuthMethod.FINGER_PRINT, authMethodDTO.getAuthMethod());
        assertEquals(CARD_NUMBER, authMethodDTO.getCardNumber());
    }

    @Test(expected = BankException.class)
    public void testFindByCardNumberNotFound() throws BankException {

        when(cardHolderRepository.findByCardNumber(CARD_NUMBER)).thenReturn(Optional.empty());

        cardHolderService.findByCardNumber(CARD_NUMBER);
    }
}