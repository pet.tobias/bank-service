package com.tobias.bankservice.service;

import com.tobias.bankservice.dto.AccountBalanceDTO;
import com.tobias.bankservice.dto.ReceiptDTO;
import com.tobias.bankservice.dto.TransactionRequestDTO;
import com.tobias.bankservice.entity.Account;
import com.tobias.bankservice.entity.CardHolder;
import com.tobias.bankservice.entity.Transaction;
import com.tobias.bankservice.entity.TransactionType;
import com.tobias.bankservice.exception.BankException;
import com.tobias.bankservice.repository.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private CardHolderService cardHolderService;

    @Mock
    private AccountRepository accountRepository;

    private final String CARD_NUMBER = "123";


    @Test
    public void testWithdrawMoneyAndNoLastTransaction() throws BankException {
        TransactionRequestDTO requestDTO = new TransactionRequestDTO(BigDecimal.TEN);
        requestDTO.setCardNumber(CARD_NUMBER);
        Account account = Account.builder()
                .balance(BigDecimal.valueOf(100))
                .build();
        CardHolder cardHolder = CardHolder.builder()
                .cardNumber(CARD_NUMBER)
                .account(account)
                .build();

        when(cardHolderService.findByCardNumber(CARD_NUMBER)).thenReturn(cardHolder);

        AccountBalanceDTO accountBalanceDTO = accountService.withdrawMoney(requestDTO);

        verify(accountRepository).save(account);
        assertEquals(BigDecimal.valueOf(90), account.getBalance());
        assertEquals(TransactionType.WITHDRAWAL, account.getLastTransaction().getTransactionType());
        assertEquals(BigDecimal.TEN, account.getLastTransaction().getAmount());
        assertEquals(BigDecimal.valueOf(90), account.getLastTransaction().getNewBalance());
        assertNotNull(accountBalanceDTO);
        assertEquals(BigDecimal.valueOf(90), accountBalanceDTO.getBalance());
    }

    @Test(expected = BankException.class)
    public void testWithdrawMoneyInsufficientFunds() throws BankException {
        TransactionRequestDTO requestDTO = new TransactionRequestDTO(BigDecimal.TEN);
        requestDTO.setCardNumber(CARD_NUMBER);
        Account account = Account.builder()
                .balance(BigDecimal.ZERO)
                .build();
        CardHolder cardHolder = CardHolder.builder()
                .cardNumber(CARD_NUMBER)
                .account(account)
                .build();

        when(cardHolderService.findByCardNumber(CARD_NUMBER)).thenReturn(cardHolder);

        accountService.withdrawMoney(requestDTO);
    }

    @Test
    public void testDepositMoneyAndLastTransaction() throws BankException {
        TransactionRequestDTO requestDTO = new TransactionRequestDTO(BigDecimal.TEN);
        requestDTO.setCardNumber(CARD_NUMBER);
        Transaction lastTransaction = Transaction.builder()
                .transactionType(TransactionType.WITHDRAWAL)
                .amount(BigDecimal.ONE)
                .newBalance(BigDecimal.ONE)
                .build();
        Account account = Account.builder()
                .balance(BigDecimal.valueOf(100))
                .lastTransaction(lastTransaction)
                .build();
        CardHolder cardHolder = CardHolder.builder()
                .cardNumber(CARD_NUMBER)
                .account(account)
                .build();

        when(cardHolderService.findByCardNumber(CARD_NUMBER)).thenReturn(cardHolder);

        AccountBalanceDTO accountBalanceDTO = accountService.depositMoney(requestDTO);

        verify(accountRepository).save(account);
        assertEquals(BigDecimal.valueOf(110), account.getBalance());
        assertEquals(TransactionType.DEPOSIT, account.getLastTransaction().getTransactionType());
        assertEquals(BigDecimal.TEN, account.getLastTransaction().getAmount());
        assertEquals(BigDecimal.valueOf(110), account.getLastTransaction().getNewBalance());
        assertNotNull(accountBalanceDTO);
        assertEquals(BigDecimal.valueOf(110), accountBalanceDTO.getBalance());
    }

    @Test
    public void testPrintReceipt() throws BankException {
        Transaction lastTransaction = Transaction.builder()
                .transactionType(TransactionType.WITHDRAWAL)
                .amount(BigDecimal.ONE)
                .newBalance(BigDecimal.ONE)
                .build();
        Account account = Account.builder()
                .lastTransaction(lastTransaction)
                .build();
        CardHolder cardHolder = CardHolder.builder()
                .cardNumber(CARD_NUMBER)
                .account(account)
                .build();

        when(cardHolderService.findByCardNumber(CARD_NUMBER)).thenReturn(cardHolder);

        ReceiptDTO receiptDTO = accountService.printReceipt(CARD_NUMBER);

        assertNotNull(receiptDTO);
        assertEquals(TransactionType.WITHDRAWAL, receiptDTO.getTransactionType());
        assertEquals(BigDecimal.ONE, receiptDTO.getNewBalance());
        assertEquals(BigDecimal.ONE, receiptDTO.getAmount());
    }

    @Test(expected = BankException.class)
    public void testPrintReceiptNoLastTransaction() throws BankException {
        Account account = Account.builder()
                .build();
        CardHolder cardHolder = CardHolder.builder()
                .cardNumber(CARD_NUMBER)
                .account(account)
                .build();

        when(cardHolderService.findByCardNumber(CARD_NUMBER)).thenReturn(cardHolder);

        accountService.printReceipt(CARD_NUMBER);
    }
}