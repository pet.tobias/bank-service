# Bank Service

## Run postgres local:
```
docker-compose -f docker-compose.yml up -d
```

## Swagger UI:
```
https://localhost:8081/swagger-ui.html
```

## Basic Auth
```
user: bank-user
pw: bank-password
```
## Entities
### CardHolder 1
```
CardNumber: 0000000000000000
PIN: 0000
FingerPrint: finger0
```
### CardHolder 2
```
CardNumber: 1111111111111111
PIN: 1111
FingerPrint: finger1
```
### CardHolder 3
```
CardNumber: 2222222222222222
PIN: 2222
FingerPrint: finger2
```
